package com.myst3ry.data.callback;

import com.myst3ry.data.datasource.local.entity.ForecastEntity;

import java.util.List;

public interface OnForecastsReceivedListener {

    void onForecastsReceived(final List<ForecastEntity> entities);
}
