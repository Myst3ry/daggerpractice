package com.myst3ry.data.callback;

import com.myst3ry.data.datasource.local.entity.ForecastEntity;

public interface OnDailyForecastReceivedListener {

    void onForecastReceived(final ForecastEntity entity);
}
