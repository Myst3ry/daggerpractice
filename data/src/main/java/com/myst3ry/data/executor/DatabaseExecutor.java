package com.myst3ry.data.executor;

import android.support.annotation.NonNull;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public final class DatabaseExecutor implements Executor {

    private final ThreadPoolExecutor mThreadPoolExecutor;

    @Inject
    public DatabaseExecutor(final JobThreadFactory factory) {
        mThreadPoolExecutor = new ThreadPoolExecutor(1, 1, 2, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(), factory);
    }

    @Override
    public void execute(@NonNull Runnable command) {
        this.mThreadPoolExecutor.execute(command);
    }
}
