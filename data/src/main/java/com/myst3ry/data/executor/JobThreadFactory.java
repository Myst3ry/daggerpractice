package com.myst3ry.data.executor;

import android.support.annotation.NonNull;

import java.util.concurrent.ThreadFactory;

import javax.inject.Inject;

public final class JobThreadFactory implements ThreadFactory {

    private int count = 0;

    @Override
    public Thread newThread(@NonNull Runnable runnable) {
        return new Thread(runnable, "request_db-" + count++);
    }
}
