package com.myst3ry.data.datasource;

import com.myst3ry.data.callback.OnDailyForecastReceivedListener;
import com.myst3ry.data.callback.OnForecastsReceivedListener;

public interface ForecastsDataSource {

    void requestAllForecasts(final String city, final OnForecastsReceivedListener callback);

    void requestDailyForecast(final int epochDate, final OnDailyForecastReceivedListener callback);
}
