package com.myst3ry.data.datasource;

import com.myst3ry.data.callback.OnDailyForecastReceivedListener;
import com.myst3ry.data.callback.OnForecastsReceivedListener;
import com.myst3ry.data.datasource.local.entity.ForecastEntity;
import com.myst3ry.data.executor.DatabaseExecutor;
import com.myst3ry.data.datasource.local.SQLiteDatabaseManager;

import java.util.List;

import javax.inject.Inject;

public final class ForecastsLocalDataSource implements ForecastsDataSource {

    private final SQLiteDatabaseManager mDatabaseManager;
    private final DatabaseExecutor mExecutor;

    @Inject
    public ForecastsLocalDataSource(final SQLiteDatabaseManager databaseManager, final DatabaseExecutor executor) {
        this.mDatabaseManager = databaseManager;
        this.mExecutor = executor;
    }

    @Override
    public void requestAllForecasts(final String city, final OnForecastsReceivedListener callback) {
        mExecutor.execute(() -> mDatabaseManager.getAllForecasts(city, callback));
    }

    @Override
    public void requestDailyForecast(final int epochDate, final OnDailyForecastReceivedListener callback) {
        mExecutor.execute(() -> mDatabaseManager.getDailyForecast(epochDate, callback));
    }

    public void saveAllForecasts(final List<ForecastEntity> entities) {
        mExecutor.execute(() -> mDatabaseManager.saveForecasts(entities));
    }

}
