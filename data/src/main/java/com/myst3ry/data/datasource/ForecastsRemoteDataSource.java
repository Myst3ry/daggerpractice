package com.myst3ry.data.datasource;

import com.myst3ry.data.callback.OnDailyForecastReceivedListener;
import com.myst3ry.data.callback.OnForecastsReceivedListener;
import com.myst3ry.data.datasource.remote.ApiMapper;

import javax.inject.Inject;

public final class ForecastsRemoteDataSource implements ForecastsDataSource {

    private final ApiMapper mApiMapper;

    @Inject
    public ForecastsRemoteDataSource(final ApiMapper apiMapper) {
        this.mApiMapper = apiMapper;
    }

    @Override
    public void requestAllForecasts(final String city, final OnForecastsReceivedListener callback) {
        mApiMapper.getForecastsRemote(city, callback);
    }

    @Override
    public void requestDailyForecast(final int epochDate, final OnDailyForecastReceivedListener callback) {
        throw new UnsupportedOperationException("Operation is not supported");
    }
}
