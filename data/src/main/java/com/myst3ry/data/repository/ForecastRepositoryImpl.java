package com.myst3ry.data.repository;

import com.myst3ry.data.NetworkManager;
import com.myst3ry.data.datasource.local.entity.ForecastEntity;
import com.myst3ry.data.datasource.local.entity.mapper.ForecastDataMapper;
import com.myst3ry.data.datasource.ForecastsDataSource;
import com.myst3ry.data.datasource.ForecastsLocalDataSource;
import com.myst3ry.data.datasource.ForecastsRemoteDataSource;
import com.myst3ry.domain.callback.OnDataListReceivedListener;
import com.myst3ry.domain.callback.OnDataReceivedListener;
import com.myst3ry.domain.repository.ForecastRepository;

import java.util.List;

import javax.inject.Inject;

public final class ForecastRepositoryImpl implements ForecastRepository {

    private final NetworkManager mNetworkManager;
    private final ForecastsLocalDataSource mLocalDataSource;
    private final ForecastsRemoteDataSource mRemoteDataSource;

    private OnDataReceivedListener mDataCallback;
    private OnDataListReceivedListener mDataListCallback;

    @Inject
    public ForecastRepositoryImpl(final NetworkManager networkManager,
                                  final ForecastsLocalDataSource localDataSource,
                                  final ForecastsRemoteDataSource remoteDataSource) {
        this.mNetworkManager = networkManager;
        this.mLocalDataSource = localDataSource;
        this.mRemoteDataSource = remoteDataSource;
    }

    @Override
    public void requestAllForecasts(final String city, OnDataListReceivedListener dataListCallback) {
        this.mDataListCallback = dataListCallback;

        ForecastsDataSource dataSource;
        if (mNetworkManager.isNetworkAvailable()) {
            dataSource = mRemoteDataSource;
        } else {
            dataSource = mLocalDataSource;
        }

        dataSource.requestAllForecasts(city, this::onForecastsReceived);
    }

    @Override
    public void requestDailyForecast(final int epochDate, OnDataReceivedListener dataCallback) {
        this.mDataCallback = dataCallback;
        mLocalDataSource.requestDailyForecast(epochDate, this::onForecastReceived);
    }

    private void onForecastsReceived(final List<ForecastEntity> entities) {
        if (entities != null) {
            mLocalDataSource.saveAllForecasts(entities);
            mDataListCallback.onDataListReceived(ForecastDataMapper.transform(entities));
        }
    }

    private void onForecastReceived(final ForecastEntity entity) {
        if (mDataCallback != null) {
            mDataCallback.onDataReceived(ForecastDataMapper.transform(entity));
        }
    }
}
