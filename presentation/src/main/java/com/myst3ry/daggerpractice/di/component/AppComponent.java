package com.myst3ry.daggerpractice.di.component;

import com.myst3ry.daggerpractice.di.module.AppModule;
import com.myst3ry.daggerpractice.di.module.DataModule;
import com.myst3ry.daggerpractice.di.module.DatabaseModule;
import com.myst3ry.daggerpractice.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (modules = {AppModule.class, DataModule.class, DatabaseModule.class, NetworkModule.class})
public interface AppComponent {

    MainSubComponent mainSubComponent();

    DetailSubComponent detailSubComponent();
}
