package com.myst3ry.daggerpractice.di.module;

import android.content.Context;

import com.myst3ry.data.executor.DatabaseExecutor;
import com.myst3ry.data.executor.JobThreadFactory;
import com.myst3ry.data.datasource.local.SQLiteDatabaseHelper;
import com.myst3ry.data.datasource.local.SQLiteDatabaseManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class DatabaseModule {

    @Provides
    @Singleton
    SQLiteDatabaseHelper providesDatabaseHelper(final Context context) {
        return new SQLiteDatabaseHelper(context);
    }

    @Provides
    @Singleton
    SQLiteDatabaseManager providesDatabaseManager(final SQLiteDatabaseHelper helper) {
        return new SQLiteDatabaseManager(helper);
    }

    @Provides
    @Singleton
    DatabaseExecutor providesDatabaseExecutor(final JobThreadFactory factory) {
        return new DatabaseExecutor(factory);
    }

    @Provides
    @Singleton
    JobThreadFactory providesJobThreadFactory() {
        return new JobThreadFactory();
    }
}
