package com.myst3ry.daggerpractice.di.component;

import com.myst3ry.daggerpractice.di.PerActivity;
import com.myst3ry.daggerpractice.di.module.ForecastsModule;
import com.myst3ry.daggerpractice.view.MainActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent (modules = {ForecastsModule.class})
public interface MainSubComponent {

    void inject(final MainActivity mainActivity);
}
