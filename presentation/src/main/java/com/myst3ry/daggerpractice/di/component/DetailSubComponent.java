package com.myst3ry.daggerpractice.di.component;

import com.myst3ry.daggerpractice.di.PerActivity;
import com.myst3ry.daggerpractice.di.module.DailyForecastModule;
import com.myst3ry.daggerpractice.view.ForecastDetailActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent (modules = {DailyForecastModule.class})
public interface DetailSubComponent {

    void inject(final ForecastDetailActivity detailActivity);
}
