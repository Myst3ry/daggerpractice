package com.myst3ry.daggerpractice.di.module;

import com.myst3ry.data.NetworkManager;
import com.myst3ry.data.datasource.ForecastsLocalDataSource;
import com.myst3ry.data.datasource.ForecastsRemoteDataSource;
import com.myst3ry.data.datasource.local.SQLiteDatabaseManager;
import com.myst3ry.data.datasource.remote.ApiMapper;
import com.myst3ry.data.executor.DatabaseExecutor;
import com.myst3ry.data.repository.ForecastRepositoryImpl;
import com.myst3ry.domain.repository.ForecastRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class DataModule {

    @Provides
    @Singleton
    ForecastsLocalDataSource providesLocalDataSource(final SQLiteDatabaseManager manager,
                                                     final DatabaseExecutor executor) {
        return new ForecastsLocalDataSource(manager, executor);
    }

    @Provides
    @Singleton
    ForecastsRemoteDataSource providesRemoteDataSource(final ApiMapper apiMapper) {
        return new ForecastsRemoteDataSource(apiMapper);
    }

    @Provides
    @Singleton
    ForecastRepository providesForecastRepository(final NetworkManager networkManager,
                                                  final ForecastsLocalDataSource localDataSource,
                                                  final ForecastsRemoteDataSource remoteDataSource) {
        return new ForecastRepositoryImpl(networkManager, localDataSource, remoteDataSource);
    }
}
