package com.myst3ry.daggerpractice.presenter;

abstract class BasePresenter<T> {

    protected T mView;

    public void attachView(T view) {
        this.mView = view;
    }

    public void detachView() {
        this.mView = null;
    }
}
