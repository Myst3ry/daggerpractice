package com.myst3ry.daggerpractice.presenter;

import com.myst3ry.daggerpractice.data.mapper.ForecastDataModelMapper;
import com.myst3ry.daggerpractice.di.PerActivity;
import com.myst3ry.daggerpractice.view.DetailView;
import com.myst3ry.domain.interactor.GetDailyForecastCase;
import com.myst3ry.domain.model.Forecast;

import javax.inject.Inject;

@PerActivity
public final class DetailPresenter extends BasePresenter<DetailView> {

    private final GetDailyForecastCase mGetDailyForecastUseCase;

    @Inject
    public DetailPresenter(final GetDailyForecastCase dailyForecastUseCase) {
        this.mGetDailyForecastUseCase = dailyForecastUseCase;
    }

    public void getForecast(final int date) {
        mGetDailyForecastUseCase.execute(date, this::onForecastReceived);
    }

    private void onForecastReceived(final Forecast forecast) {
        mView.setForecastInfo(ForecastDataModelMapper.transform(forecast));
    }
}
