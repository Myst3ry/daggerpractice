package com.myst3ry.daggerpractice.view;

import com.myst3ry.daggerpractice.data.model.ForecastDataModel;

import java.util.List;

public interface MainView {

    void setForecasts(final List<ForecastDataModel> forecasts);

    void showProgressBar();

    void hideProgressBar();

    void hideRefresher();

    void showError();

    void setDaysCount(final String days);
}
