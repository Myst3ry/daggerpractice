package com.myst3ry.daggerpractice.view;

import com.myst3ry.daggerpractice.data.model.ForecastDataModel;

public interface DetailView {

    void setForecastInfo(final ForecastDataModel forecast);
}
