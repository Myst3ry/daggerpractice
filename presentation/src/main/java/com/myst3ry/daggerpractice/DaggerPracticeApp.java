package com.myst3ry.daggerpractice;

import android.app.Application;

import com.myst3ry.daggerpractice.di.component.AppComponent;
import com.myst3ry.daggerpractice.di.component.DaggerAppComponent;
import com.myst3ry.daggerpractice.di.module.AppModule;

public final class DaggerPracticeApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        prepareDaggerComponents();
    }

    private void prepareDaggerComponents() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
