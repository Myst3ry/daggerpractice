package com.myst3ry.daggerpractice;

public interface OnForecastClickListener {

    void onForecastClick(final int id);
}
